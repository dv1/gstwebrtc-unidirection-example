This is a simple example for how to write a WebRTC sender with a self-contained signaling server.
The use case for such a setup is when one wants to use WebRTC unidirectionally for low-latency video playback in HTML5.

This is currently WIP.

Build with meson:

    mkdir build
    cd build
    meson ..

Then, run this in the build/ directory:

    ./gstwebrtc-unidirectional-example ../webrtc-page.html
