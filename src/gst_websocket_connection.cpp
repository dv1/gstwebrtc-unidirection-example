#include <iostream>
#include "json/json.hpp"
#include "gst_websocket_connection.hpp"
#include "scope_guard.hpp"

#define GST_USE_UNSTABLE_API
#include <gst/webrtc/webrtc.h>


gst_websocket_connection::gst_websocket_connection(SoupWebsocketConnection *soup_connection)
	: websocket_connection(soup_connection)
{
}


gst_websocket_connection::~gst_websocket_connection()
{
	if (m_pipeline != nullptr)
	{
		std::cerr << "Stopping and tearing down WebRTC receiver pipeline\n";

		gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_NULL);

		gst_object_unref(GST_OBJECT(m_webrtcbin));
		gst_object_unref(GST_OBJECT(m_payloader));
		gst_object_unref(GST_OBJECT(m_pipeline));
	}
}


bool gst_websocket_connection::initialize()
{
	GError *error;

	error = nullptr;
	m_pipeline = gst_parse_launch("webrtcbin name=webrtcbin "
		"videotestsrc pattern=ball ! vp8enc ! "
		"rtpvp8pay name=payloader ! queue ! "
		"application/x-rtp,media=video,encoding-name=VP8,payload=96 ! "
		"webrtcbin. ",
		&error
	);
	if (error != nullptr)
	{
		std::cerr << "Could not create WebRTC receiver pipeline: " << error->message << "\n";
		g_error_free(error);
		return false;
	}

	m_webrtcbin = gst_bin_get_by_name(GST_BIN(m_pipeline), "webrtcbin");
	m_payloader = gst_bin_get_by_name(GST_BIN(m_pipeline), "payloader");
	g_assert(m_webrtcbin != nullptr);
	g_assert(m_payloader != nullptr);

	if (!gst_element_set_state(m_pipeline, GST_STATE_PLAYING))
	{
		std::cerr << "Could not start WebRTC receiver pipeline\n";
		return false;
	}

	g_signal_connect(m_webrtcbin, "on-negotiation-needed", G_CALLBACK(on_negotiation_needed_cb), gpointer(this));
	g_signal_connect(m_webrtcbin, "on-ice-candidate", G_CALLBACK(on_ice_candidate_cb), gpointer(this));

	GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(m_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "webrtc-receiver-pipeline");

	std::cerr << "Started WebRTC receiver pipeline\n";

	return true;
}


void gst_websocket_connection::handle_new_message(SoupWebsocketDataType data_type, GBytes *message)
{
	int ret;
	std::string_view json_string;
	GstSDPMessage *sdp_message = nullptr;
	GstWebRTCSessionDescription *answer = nullptr;

	auto guard = make_scope_guard([&]() {
		if (sdp_message != nullptr)
			gst_sdp_message_free(sdp_message);
	});

	switch (data_type)
	{
		case SOUP_WEBSOCKET_DATA_BINARY:
			std::cerr << "Received unknown binary message, ignoring\n";
			return;

		case SOUP_WEBSOCKET_DATA_TEXT:
		{
			gsize data_size;
			gconstpointer data_ptr;
			data_ptr = g_bytes_get_data(message, &data_size);
			json_string = std::string_view(reinterpret_cast<char const *>(data_ptr), data_size);
			break;
		}

		default:
			g_assert_not_reached();
	}

	std::cerr << "Message JSON:\n";
	std::cerr << json_string << "\n";

	nlohmann::json message_json = nlohmann::json::parse(json_string);
	std::string msg_type_string = message_json["type"].get<std::string>();
	nlohmann::json &data_json = message_json["data"];

	if (msg_type_string == "sdp")
	{
		std::string sdp_type_string = data_json["type"].get<std::string>();
		if (sdp_type_string != "answer")
		{
			std::cerr << "Expected SDP message type \"answer\", got \"" << sdp_type_string << "\"\n";
			return;
		}

		std::string sdp_string = data_json["sdp"].get<std::string>();

		std::cerr << "Received SDP:\n";
		std::cerr << sdp_string << "\n";

		ret = gst_sdp_message_new(&sdp_message);
		g_assert_cmphex(ret, ==, GST_SDP_OK);

		ret = gst_sdp_message_parse_buffer(reinterpret_cast<guint8 const *>(sdp_string.c_str()), sdp_string.length(), sdp_message);
		if (ret != GST_SDP_OK)
		{
			std::cerr << "Could not parse SDP string\n";
			return;
		}

		answer = gst_webrtc_session_description_new(GST_WEBRTC_SDP_TYPE_ANSWER, sdp_message);
		g_assert_nonnull(answer);

		GstPromise *promise = gst_promise_new();
		g_signal_emit_by_name(m_webrtcbin, "set-remote-description", answer, promise);
		gst_promise_interrupt(promise);
		gst_promise_unref(promise);
	}
	else if (msg_type_string == "ice")
	{
		int mline_index = data_json["sdpMLineIndex"].get<int>();
		std::string candidate_string = data_json["candidate"].get<std::string>();

		std::cerr << "Received ICE candidate with mline index " << mline_index << "; candidate: " << candidate_string << "\n";

		g_signal_emit_by_name(m_webrtcbin, "add-ice-candidate", mline_index, candidate_string.c_str());
	}
	else
	{
		std::cerr << "Unknown message type \"" << msg_type_string << "\"\n";
		return;
	}
}


void gst_websocket_connection::on_offer_created_cb(GstPromise *promise, gpointer user_data)
{
	gst_websocket_connection *self = reinterpret_cast<gst_websocket_connection *>(user_data);

	GstWebRTCSessionDescription *offer = nullptr;
	GstStructure const *reply;
	GstPromise *local_desc_promise;

	reply = gst_promise_get_reply(promise);
	gst_structure_get(reply, "offer", GST_TYPE_WEBRTC_SESSION_DESCRIPTION, &offer, nullptr);
	gst_promise_unref(promise);

	local_desc_promise = gst_promise_new();
	g_signal_emit_by_name(self->m_webrtcbin, "set-local-description", offer, local_desc_promise);
	gst_promise_interrupt(local_desc_promise);
	gst_promise_unref(local_desc_promise);

	gchar *sdp_cstring = gst_sdp_message_as_text(offer->sdp);
	std::cerr << "Negotiation offer created:\n";
	std::cerr << sdp_cstring << "\n";

	nlohmann::json sdp_json = {
		{ "type", "sdp" },
		{ "data", {
			{ "type", "offer" },
			{ "sdp", sdp_cstring }
		}}
	};

	g_free(sdp_cstring);

	soup_websocket_connection_send_text(self->m_soup_connection, sdp_json.dump().c_str());

	gst_webrtc_session_description_free(offer);
}


void gst_websocket_connection::on_negotiation_needed_cb(GstElement *webrtcbin, gpointer user_data)
{
	std::cerr << "Creating WebRTC negotiation offer\n";
	GstPromise *promise = gst_promise_new_with_change_func(on_offer_created_cb, user_data, nullptr);
	g_signal_emit_by_name(G_OBJECT(webrtcbin), "create-offer", nullptr, promise);
}


void gst_websocket_connection::on_ice_candidate_cb(GstElement *, guint mline_index, gchar *candidate, gpointer user_data)
{
	gst_websocket_connection *self = reinterpret_cast<gst_websocket_connection *>(user_data);

	nlohmann::json ice_json = {
		{ "type", "ice" },
		{ "data", {
			{ "sdpMLineIndex", mline_index },
			{ "candidate", candidate }
		}}
	};

	soup_websocket_connection_send_text(self->m_soup_connection, ice_json.dump().c_str());
}


