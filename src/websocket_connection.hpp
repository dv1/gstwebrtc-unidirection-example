#ifndef WEBSOCKET_CONNECTION_HPP
#define WEBSOCKET_CONNECTION_HPP

#include <memory>
#include <libsoup/soup.h>


class websocket_connection
{
public:
	virtual ~websocket_connection();

	virtual bool initialize() = 0;
	virtual void handle_new_message(SoupWebsocketDataType data_type, GBytes *message) = 0;

protected:
	websocket_connection(SoupWebsocketConnection *soup_connection);

	SoupWebsocketConnection *m_soup_connection;
};

typedef std::unique_ptr<websocket_connection> websocket_connection_uptr;


#endif // WEBSOCKET_CONNECTION_HPP
