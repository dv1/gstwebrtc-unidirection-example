#ifndef GST_WEBSOCKET_CONNECTION_HPP
#define GST_WEBSOCKET_CONNECTION_HPP

#include <gst/gst.h>
#include "websocket_connection.hpp"


class gst_websocket_connection
	: public websocket_connection
{
public:
	explicit gst_websocket_connection(SoupWebsocketConnection *soup_connection);
	virtual ~gst_websocket_connection();

	virtual bool initialize() override;
	virtual void handle_new_message(SoupWebsocketDataType data_type, GBytes *message) override;


private:
	static void on_offer_created_cb(GstPromise *promise, gpointer user_data);
	static void on_negotiation_needed_cb(GstElement *webrtcbin, gpointer user_data);
	static void on_ice_candidate_cb(GstElement *webrtcbin, guint mline_index, gchar *candidate, gpointer user_data);

	GstElement *m_pipeline = nullptr;
	GstElement *m_webrtcbin = nullptr;
	GstElement *m_payloader = nullptr;
};


#endif // GST_WEBSOCKET_CONNECTION_HPP
