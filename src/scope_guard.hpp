#ifndef SCOPE_GUARD_HPP
#define SCOPE_GUARD_HPP

#include <functional>


namespace detail
{


class scope_guard_impl
{
public:
	template<typename Func>
	explicit scope_guard_impl(Func &&func)
		: m_func(std::forward<Func>(func))
		, m_dismissed(false)
	{
	}

	~scope_guard_impl()
	{
		if (!m_dismissed)
		{
			// Make sure exceptions never exit the destructor, otherwise
			// undefined behavior occurs. For details about this, see
			// http://bin-login.name/ftp/pub/docs/programming_languages/cpp/cffective_cpp/MEC/MI11_FR.HTM
			try
			{
				m_func();
			}
			catch (...)
			{
			}
		}
	}

	scope_guard_impl(scope_guard_impl &&other)
		: m_func(std::move(other.m_func))
		, m_dismissed(other.m_dismissed)
	{
		other.m_dismissed = true;
	}

	void dismiss() const throw()
	{
		m_dismissed = true;
	}


private:
	scope_guard_impl(scope_guard_impl const &) = delete;
	scope_guard_impl& operator = (scope_guard_impl const &) = delete;

	std::function<void()> m_func;
	mutable bool m_dismissed;
};


} // namespace detail end


template<typename Func>
detail::scope_guard_impl make_scope_guard(Func &&func)
{
	return detail::scope_guard_impl(std::forward<Func>(func));
}


#endif // SCOPE_GUARD_HPP
