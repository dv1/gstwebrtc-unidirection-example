#include <iostream>
#include <glib.h>
#include <glib-unix.h>
#include <gst/gst.h>
#include "soup_webserver.hpp"
#include "gst_websocket_connection.hpp"


gboolean exit_sighandler(gpointer user_data)
{
	std::cerr << "Caught signal, stopping mainloop\n";
	GMainLoop *mainloop = reinterpret_cast<GMainLoop*>(user_data);
	g_main_loop_quit(mainloop);
	return TRUE;
}


int main(int argc, char **argv)
{
	if (argc < 2)
	{
		std::cerr << "Usage: " << argv[0] << " <HTML filename>\n";
		return -1;
	}

	GError *error = nullptr;
	if (!gst_init_check(&argc, &argv, &error))
	{
		std::cerr << "Could not initialize GStreamer: " << error->message << "\n";
		g_error_free(error);
		return -1;
	}

	GMainLoop *mainloop;
	mainloop = g_main_loop_new(nullptr, FALSE);
  	g_assert(mainloop != nullptr);

	g_unix_signal_add(SIGINT, exit_sighandler, mainloop);
	g_unix_signal_add(SIGTERM, exit_sighandler, mainloop);

	{
		soup_webserver webserver(argv[1]);
		webserver.listen([](SoupWebsocketConnection *soup_connection) {
			return websocket_connection_uptr(new gst_websocket_connection(soup_connection));
		});

		g_main_loop_run(mainloop);
	}

	g_main_loop_unref(mainloop);

	gst_deinit();

	return 0;
}
