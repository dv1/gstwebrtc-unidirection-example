#include <cstddef>
#include <iostream>
#include <fstream>
#include "soup_webserver.hpp"


static int const SOUP_HTTP_PORT = 57778;


soup_webserver::soup_webserver(std::string index_html_filename)
	: m_index_html_filename(index_html_filename)
{
	m_soup_server = soup_server_new(SOUP_SERVER_SERVER_HEADER, "webrtc-soup-server", nullptr);
	soup_server_add_handler(m_soup_server, "/", soup_http_handler, gpointer(this), nullptr);
	soup_server_add_websocket_handler(m_soup_server, "/ws", nullptr, nullptr, soup_websocket_handler, gpointer(this), nullptr);
}


soup_webserver::~soup_webserver()
{
	// Clear the websocket connection first to have their destructors
	// be called _before_ the webserver is shut down below.
	m_websocket_connections.clear();

	if (m_soup_server != nullptr)
		g_object_unref(G_OBJECT(m_soup_server));
}


void soup_webserver::listen(create_websocket_connection_cb on_new_connection)
{
	m_on_new_connection = std::move(on_new_connection);
	std::cerr << "Listening on port " << SOUP_HTTP_PORT << " and path \"/\" for HTTP requests/\n";
	soup_server_listen_all(m_soup_server, SOUP_HTTP_PORT, (SoupServerListenOptions)0, nullptr);
}


void soup_webserver::soup_http_handler(
	SoupServer *,
	SoupMessage *message,
	const char *path,
	GHashTable *,
	SoupClientContext *,
	gpointer user_data
)
{
	SoupBuffer *soup_buffer;
	soup_webserver *self = reinterpret_cast<soup_webserver *>(user_data);

	if ((g_strcmp0(path, "/") != 0) && (g_strcmp0(path, "/index.html") != 0))
	{
		soup_message_set_status(message, SOUP_STATUS_NOT_FOUND);
		return;
	}

	std::ifstream index_file(self->m_index_html_filename.c_str());
	if (!index_file)
	{
		std::cerr << "Could not find HTML file \"" << self->m_index_html_filename.c_str() << "\"\n";
		soup_message_set_status(message, SOUP_STATUS_SERVICE_UNAVAILABLE);
		return;
	}

	std::string html_source;

	while (index_file)
	{
		std::string line;
		std::getline(index_file, line, '\n');
		html_source += line + "\n";
	}

	soup_buffer = soup_buffer_new(SOUP_MEMORY_TEMPORARY, html_source.data(), html_source.length());
	soup_message_headers_set_content_type(message->response_headers, "text/html", nullptr);
	soup_message_body_append_buffer(message->response_body, soup_buffer);
	soup_buffer_free(soup_buffer);

	soup_message_set_status(message, SOUP_STATUS_OK);
}


void soup_webserver::soup_websocket_handler(
	SoupServer *,
	SoupWebsocketConnection *soup_connection,
	const char *,
	SoupClientContext *,
	gpointer user_data
)
{
	soup_webserver *self = reinterpret_cast<soup_webserver *>(user_data);

	std::cerr << "Processing new websocket connection " << std::hex << std::uintptr_t(soup_connection) << std::dec << "\n";

	auto new_websocket_connection = self->m_on_new_connection(soup_connection);

	if (!new_websocket_connection->initialize())
	{
		std::cerr << "Could not setup websocket connection\n";
		return;
	}

	g_signal_connect(G_OBJECT(soup_connection), "message", G_CALLBACK(soup_websocket_message), gpointer(new_websocket_connection.get()));
	g_signal_connect(G_OBJECT(soup_connection), "closed", G_CALLBACK(soup_websocket_closed), gpointer(self));

	self->m_websocket_connections.emplace(soup_connection, std::move(new_websocket_connection));
}


void soup_webserver::soup_websocket_message(SoupWebsocketConnection *, SoupWebsocketDataType data_type, GBytes *message, gpointer user_data)
{
	websocket_connection *connection = reinterpret_cast<websocket_connection *>(user_data);
	connection->handle_new_message(data_type, message);
}


void soup_webserver::soup_websocket_closed(SoupWebsocketConnection *soup_connection, gpointer user_data)
{
	soup_webserver *self = reinterpret_cast<soup_webserver *>(user_data);
	self->m_websocket_connections.erase(soup_connection);
	std::cerr << "Closed websocket connection " << std::hex << std::uintptr_t(soup_connection) << std::dec << "\n";
}
