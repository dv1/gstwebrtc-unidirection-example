#include "gst_websocket_connection.hpp"


websocket_connection::websocket_connection(SoupWebsocketConnection *soup_connection)
	: m_soup_connection(soup_connection)
{
	g_object_ref(G_OBJECT(soup_connection));
}


websocket_connection::~websocket_connection()
{
	if (m_soup_connection != nullptr)
		g_object_unref(G_OBJECT(m_soup_connection));
}
