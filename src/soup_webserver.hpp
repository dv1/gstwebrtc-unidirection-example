#ifndef SOUP_WEBSERVER_HPP
#define SOUP_WEBSERVER_HPP

#include <memory>
#include <functional>
#include <map>
#include <libsoup/soup.h>
#include "websocket_connection.hpp"


class soup_webserver
{
public:
	typedef std::function<websocket_connection_uptr(SoupWebsocketConnection *soup_connection)> create_websocket_connection_cb;

	explicit soup_webserver(std::string index_html_filename);
	~soup_webserver();

	void listen(create_websocket_connection_cb on_new_connection);

private:
	static void soup_http_handler(
		SoupServer *,
		SoupMessage *message,
		const char *path,
		GHashTable *,
		SoupClientContext *,
		gpointer user_data
	);
	static void soup_websocket_handler(
		SoupServer *,
		SoupWebsocketConnection *soup_connection,
		const char *,
		SoupClientContext *,
		gpointer user_data
	);
	static void soup_websocket_message(SoupWebsocketConnection *, SoupWebsocketDataType data_type, GBytes *message, gpointer user_data);
	static void soup_websocket_closed(SoupWebsocketConnection *soup_connection, gpointer user_data);

	SoupServer *m_soup_server;
	std::map<SoupWebsocketConnection *, websocket_connection_uptr> m_websocket_connections;
	std::string m_index_html_filename;
	create_websocket_connection_cb m_on_new_connection;
};


#endif // SOUP_WEBSERVER_HPP
